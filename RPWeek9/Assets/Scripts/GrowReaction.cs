﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Reactions/Grow")]
public class GrowReaction : Reaction
{
    public float GrowAmount;
    public float GrowRate;
    public float GrowDuration;
    public override void React(Transform molucule1, Liquid liquid1, Transform molucule2, Liquid liquid2)
    {
        liquid1.Grow(GrowAmount,GrowRate, GrowDuration);
    }

  
}
