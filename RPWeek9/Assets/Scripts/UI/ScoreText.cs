﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreText : MonoBehaviour
{
    public string preScoreText = "You have ";
    Text text;

    // Start is called before the first frame update
    void Start()
    {
        text = GetComponent<Text>();

        Liquid.OnColorChanged += UpdateScore;
        text.text = preScoreText + Gamemanager.instance.colorAmounts[Gamemanager.instance.TargetType] + "/" + Gamemanager.instance.TargetAmount + " of " + Gamemanager.instance.TargetType.ToString() + " orbs";
    }

    void UpdateScore()
    {
        text.text = preScoreText + Gamemanager.instance.colorAmounts[Gamemanager.instance.TargetType] + "/" + Gamemanager.instance.TargetAmount + " of " + Gamemanager.instance.TargetType.ToString() + " orbs";
    }

    private void OnDisable()
    {
        Liquid.OnColorChanged -= UpdateScore;
    }
}
