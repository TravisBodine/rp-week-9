﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestTubeController : MonoBehaviour
{
    public float moveSpeed;
    public float rotateSpeed;

    Transform tf;
    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        float HorzAxis = Input.GetAxis("Horizontal");
        float VertAxis = Input.GetAxis("Vertical");
        if (HorzAxis !=0)
        {
            tf.position +=(Vector3.right * moveSpeed * HorzAxis * Time.deltaTime);
        }
        if (VertAxis != 0)
        {
            tf.position +=(Vector3.up * moveSpeed * VertAxis * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.Q))
        {
            tf.Rotate(Vector3.forward * 1, rotateSpeed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.E))
        {
            tf.Rotate(Vector3.forward * -1, rotateSpeed * Time.deltaTime);
        }
    }
}
