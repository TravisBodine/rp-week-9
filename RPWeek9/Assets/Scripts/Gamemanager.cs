﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gamemanager : MonoBehaviour
{
    public static Gamemanager instance;
    public List<Liquid> molucules;

    public MenuManager menuManager;

    public Liquid.LiquidType TargetType;
    public int TargetAmount;

    public int MaxLife;
    [HideInInspector]
    public int life;

    public  Dictionary<Liquid.LiquidType, int> colorAmounts = new Dictionary<Liquid.LiquidType, int>()
    {
        { Liquid.LiquidType.black ,0},
        { Liquid.LiquidType.blue ,0},
        { Liquid.LiquidType.green ,0},
        { Liquid.LiquidType.red ,0},
        { Liquid.LiquidType.yellow ,0 }

    };

    // Start is called before the first frame update
    void Awake()
    {
        if(instance == null)
        {
            instance = this;
            life = MaxLife;
            DontDestroyOnLoad(gameObject);
            Liquid.OnColorChanged += CheckForWin;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void CheckForWin()
    {
        if(colorAmounts[TargetType] > TargetAmount)
        {
            Time.timeScale = 0;
            menuManager.ChangeMenu("WinScreen");

        }
    }
    public void CheckForloss()
    {
        if(life <= 0)
        {
            Time.timeScale = 0;
            menuManager.ChangeMenu("LoseScreen");
        }
    }

    internal void Reset()
    {
        Time.timeScale = 1;
        life = MaxLife;
        colorAmounts = new Dictionary<Liquid.LiquidType, int>()
        {
         { Liquid.LiquidType.black ,0},
         { Liquid.LiquidType.blue ,0},
         { Liquid.LiquidType.green ,0},
         { Liquid.LiquidType.red ,0},
         { Liquid.LiquidType.yellow ,0 }

        };

    }
}
