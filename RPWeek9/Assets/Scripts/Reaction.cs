﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Reaction : ScriptableObject
{
    public Liquid.LiquidType typeToReactWith;

    public bool ReactWithAny = false;

    public abstract void React(Transform molucule1,Liquid liquid1, Transform molucule2, Liquid liquid2);
   
}
