﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoluculeSpawner : MonoBehaviour
{

    public Liquid.LiquidType TypeToSpawn;

    public GameObject MoluculeTOSpawn;
    public float SpawnRate;
    float timer = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        

        if (timer < Time.time)
        {
            Spawn();
            timer = Time.time + SpawnRate;
        }
    }

    void Spawn()
    {
        Instantiate(MoluculeTOSpawn, transform.position, Quaternion.identity);
    }
}
