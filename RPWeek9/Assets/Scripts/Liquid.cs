﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Liquid : MonoBehaviour
{

    public enum LiquidType { blue, green, yellow, red, black };
    public LiquidType type;

    static Dictionary<LiquidType, Color> colors = new Dictionary<LiquidType, Color>()
    {
        { LiquidType.blue,Color.blue},
        { LiquidType.green,Color.green},
        { LiquidType.yellow,Color.yellow},
        { LiquidType.red,Color.red},
        { LiquidType.black,Color.black}
            
    };

    public List<Reaction> reactions;

    public static event Action OnColorChanged = delegate { };

    SpriteRenderer sr;
    Transform tf;
    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>();
        sr = GetComponent<SpriteRenderer>();
        Gamemanager.instance.colorAmounts[type]++;
        sr.color = colors[type];
        Gamemanager.instance.molucules.Add(this);
        OnColorChanged();
    }

    public void ChangeColor()
    {
       
        sr.color = colors[type];
        OnColorChanged();
    }

    //i wanted this in the scriptable object but it dosent like corotines
    public void Grow(float amount,float rate,float duration)
    {
        StartCoroutine(grow(amount, rate,duration));
    }

    IEnumerator grow(float amount,float rate, float duration)
    {
        float timer = Time.time + duration;
        float timeElapsed = 0;
        while (timer > Time.time)
        {
            timeElapsed += Time.deltaTime;
            Debug.Log((timeElapsed/duration) / 1-amount);
            rate = (timeElapsed/duration) / 1-amount;
            tf.localScale = new Vector3(Mathf.Lerp(tf.localScale.x, tf.localScale.x + amount, rate), Mathf.Lerp(tf.localScale.y, tf.localScale.y + amount, rate), Mathf.Lerp(tf.localScale.z, tf.localScale.z + amount, rate));
            //tf.localScale.Set(Mathf.Lerp(tf.localScale.x, tf.localScale.x + amount, rate), Mathf.Lerp(tf.localScale.y, tf.localScale.y + amount, rate), Mathf.Lerp(tf.localScale.z, tf.localScale.z + amount, rate));
            yield return null;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Liquid liquid = collision.gameObject.GetComponent<Liquid>();

        if(liquid == null || liquid.type == type)
        {
            return;
        }

        foreach  (Reaction reaction in reactions)
        {
            if(reaction.typeToReactWith == liquid.type|| reaction.ReactWithAny)
            {
                reaction.React(tf,this,collision.gameObject.transform,liquid);
            }
        }
    }

    private void OnDestroy()
    {
        Gamemanager.instance.molucules.Remove(this);
    }
}
