﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Reactions/Explosion")]
public class ExplosionReaction : Reaction { 
    // Start is called before the first frame update
   
    public override void React(Transform molucule1, Liquid liquid1, Transform molucule2, Liquid liquid2)
    {
        Gamemanager.instance.life--;
        Destroy(molucule1.gameObject);
        Destroy(molucule2.gameObject);
        Gamemanager.instance.CheckForloss();
    }
}
