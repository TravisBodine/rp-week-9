﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Reactions/ChangeColor")]
public class ChangeColorReaction : Reaction
{
    public Liquid.LiquidType colorToChangeTo;
    public bool copyColor = false;
    public override void React(Transform molucule1, Liquid liquid1, Transform molucule2, Liquid liquid2)
    {
        Gamemanager.instance.colorAmounts[liquid1.type]--;
        if (copyColor)
        {
            liquid1.type = liquid2.type;
        }
        else
        {
            liquid1.type = colorToChangeTo;
        }
        Gamemanager.instance.colorAmounts[liquid1.type]++;
        liquid1.ChangeColor();
    }

}
